<?php

namespace Doof\Validate;

use Doof\Validate\Contracts\RuleFactoryInterface;
use Doof\Validate\Rules\Boolean;
use Doof\Validate\Rules\Email;
use Doof\Validate\Rules\Float;
use Doof\Validate\Rules\Integer;
use Doof\Validate\Rules\IP;
use Doof\Validate\Rules\Mac;
use Doof\Validate\Rules\URL;

/**
 * Class FilterVarFactory
 * @package Doof\Validate
 */
class FilterVarFactory implements RuleFactoryInterface
{

    /**
     * @param mixed $rule
     * @return \Doof\Validate\Rules\Boolean|
     *         \Doof\Validate\Rules\Email|
     *         \Doof\Validate\Rules\Float|
     *         \Doof\Validate\Rules\Integer|
     *         \Doof\Validate\Rules\IP|
     *         \Doof\Validate\Rules\Mac|
     *         \Doof\Validate\Rules\URL
     */
    public function getRule($rule)
    {
        switch ($rule)
        {
            case 'boolean':
                return new Boolean;
                break;
            case 'email':
                return new Email;
                break;
            case 'float':
                return new Float;
                break;
            case 'int':
                return new Integer;
                break;
            case 'ip':
                return new IP;
                break;
            case 'mac':
                return new Mac;
                break;
            case 'url':
                return new URL;
                break;
        }
    }

}