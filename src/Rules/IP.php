<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class IP
 * @package Doof\Validate\Rules
 */
class IP extends FilterVar
{

    /**
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct()
    {
        parent::__construct('ip');
    }

}