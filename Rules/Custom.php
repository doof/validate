<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\Rule;

/**
 * Class Custom
 * @package Doof\Validate\Rules
 */
class Custom extends Rule
{

    /**
     * @param \Closure $rule
     */
    public function __construct(\Closure $rule)
    {
        parent::__construct($rule);
    }

    /**
     * @param mixed $input - The input given
     * @return bool - The value cast as a boolean.
     */
    public function isValid($input)
    {
        $func = $this->rule;
        return (bool) $func($input);
    }

    /**
     * @param mixed $input
     * @return mixed - The raw return value.
     */
    public function isValidRaw($input)
    {
        $func = $this->rule;
        return $func($input);
    }

}