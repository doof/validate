<?php

namespace Doof\Validate;

/**
 * Class Validator
 * @package Doof\Validate
 */
class Validator
{

    /** @var array */
    private $rules;

    /** @var array */
    private $input;

    /** @var \Doof\Validate\RuleFactory */
    private $factory;

    /** @var \Doof\Validate\RuleContainer */
    private $container;

    /**
     * @param array $rules
     * @param array $input
     */
    public function __construct($rules = [], $input = [])
    {
        $this->rules = $rules;
        $this->input = $input;
        $this->factory = new RuleFactory(new FilterVarFactory);
        $this->container = new RuleContainer;
        $this->initRules();
    }

    /**
     * @param mixed[] $input
     */
    public function addInput($input)
    {
        $this->input = array_merge($this->input, $input);
    }

    /**
     * @param mixed[] $rules
     */
    public function addRules($rules)
    {
        $this->rules = array_merge($this->rules, $rules);
        $this->initRules();
    }

    /**
     * This will initialize the rules
     */
    private function initRules()
    {
        foreach ($this->rules as $key => $value)
        {
            $this->container->{$key} = $this->factory->getRule($value);
        }
    }

    /**
     * @return bool - Whether or not the input is valid
     */
    public function isValid()
    {
        if (!empty($this->input))
        {
            foreach ($this->input as $key => $value)
            {
                if (!$this->container->{$key}->isValid($value))
                {
                    return false;
                }
            }
        }

        return true;
    }

}