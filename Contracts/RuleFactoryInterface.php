<?php

namespace Doof\Validate\Contracts;

/**
 * Interface RuleFactoryInterface
 * @package Doof\Validate\Contracts
 */
interface RuleFactoryInterface
{

    /**
     * @param mixed $rule
     * @return \Doof\Validate\Rules\Rule
     */
    public function getRule($rule);

}