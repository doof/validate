<?php

namespace Doof\Validate\Rules;

/**
 * Class Rule
 * @package Doof\Validate\Rules
 */
abstract class Rule
{

    /** @var string|\mixed[]|\Closure $rule */
    protected $rule;

    /** @var string $error - The error message */
    protected $error = '';

    /**
     * @param string|\mixed[]|\Closure $rule
     * @param string $message - The error message
     */
    public function __construct($rule, $message = '')
    {
        $this->rule = $rule;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return get_called_class();
    }

    /**
     * @return string|\mixed[]|\Closure
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @return string - The error message
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $input - The input given
     * @return bool
     */
    abstract public function isValid($input);

}