<?php

namespace Doof\Validate;

use Doof\Validate\Rules\Rule;

/**
 * Class RuleContainer
 * @package Doof\Validate
 */
class RuleContainer
{

    /** @var \Doof\Validate\Rules\Rule[] */
    private $rules;

    /**
     * @param \Doof\Validate\Rules\Rule[] $rules
     */
    public function __construct($rules = [])
    {
        if (!empty($rules))
        {
            $this->rules = $rules;
        }
    }

    /**
     * @param string $name
     * @return \Doof\Validate\Rules\Rule
     */
    public function __get($name)
    {
        return $this->rules[$name];
    }

    /**
     * @param string $name
     * @param \Doof\Validate\Rules\Rule $value
     */
    public function __set($name, Rule $value)
    {
        $this->rules[$name] = $value;
    }

    /**
     * @param string $name
     * @param \Doof\Validate\Rules\Rule $value
     */
    public function addRule($name, Rule $value)
    {
        $this->__set($name, $value);
    }

    /**
     * @param string $name
     * @return Rule
     */
    public function getRule($name)
    {
        return $this->__get($name);
    }

}