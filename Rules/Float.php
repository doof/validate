<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class Float
 * @package Doof\Validate\Rules
 */
class Float extends FilterVar
{

    /**
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct()
    {
        parent::__construct('float');
    }

}