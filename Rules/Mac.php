<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class Mac
 * @package Doof\Validate\Rules
 */
class Mac extends FilterVar
{

    /**
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct()
    {
        parent::__construct('mac');
    }

}