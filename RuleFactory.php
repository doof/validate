<?php

namespace Doof\Validate;

use Doof\Validate\Contracts\RuleFactoryInterface;
use Doof\Validate\Rules\Custom;
use Doof\Validate\Rules\FilterVar;
use Doof\Validate\Rules\Option;
use Doof\Validate\Rules\Text;

class RuleNotFoundException extends \Exception {}

/**
 * Class RuleFactory
 * @package Doof\Validate
 */
class RuleFactory implements RuleFactoryInterface
{

    /** @var \Doof\Validate\FilterVarFactory */
    private $factory;

    /**
     * @param \Doof\Validate\FilterVarFactory $factory
     */
    public function __construct(FilterVarFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * This method will return a rule based on the type of input received.
     *
     * @param string|mixed[]|Closure $rule
     * @return \Doof\Validate\Rules\Rule
     * @throws \Doof\Validate\RuleNotFoundException
     */
    public function getRule($rule)
    {
        if (is_string($rule))
        {
            if (FilterVar::isValidFilter($rule))
            {
                return $this->factory->getRule($rule);
            }
            else
            {
                return new Text($rule);
            }
        }
        elseif (is_array($rule))
        {
            return new Option($rule);
        }
        elseif (is_callable($rule))
        {
            return new Custom($rule);
        }
        else
        {
            throw new RuleNotFoundException;
        }
    }

}