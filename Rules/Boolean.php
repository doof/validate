<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class Boolean
 * @package Doof\Validate\Rules
 */
class Boolean extends FilterVar
{

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct('boolean');
    }

}