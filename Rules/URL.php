<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class URL
 * @package Doof\Validate\Rules
 */
class URL extends FilterVar
{

    /**
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct()
    {
        parent::__construct('url');
    }

}