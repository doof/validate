<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\Rule;

/**
 * Class Text
 * @package Doof\Validate\Rules
 */
class Text extends Rule
{

    /**
     * @param string $rule
     */
    public function __construct($rule)
    {
        parent::__construct(substr($rule, 0, 1) === '/' && substr($rule, -1) === '/' ? $rule : "/^$rule$/");
    }

    /**
     * @param string $input - The input given
     * @return bool
     */
    public function isValid($input)
    {
        return (bool) preg_match($this->rule, $input);
    }

}