<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class Integer
 * @package Doof\Validate\Rules
 */
class Integer extends FilterVar
{

    /**
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct()
    {
        parent::__construct('int');
    }

}