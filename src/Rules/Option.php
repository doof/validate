<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\Rule;

/**
 * Class Option
 * @package Doof\Validate\Rules
 */
class Option extends Rule
{

    /**
     * @param string[] $rule
     */
    public function __construct(array $rule)
    {
        parent::__construct($rule);
    }

    /**
     * @param string $input
     * @return bool
     */
    public function isValid($input)
    {
        return in_array($input, $this->rule);
    }

}