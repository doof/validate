<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\Rule;

/**
 * Class InvalidFilterException
 * @package Doof\Validate\Rules
 */
class InvalidFilterException extends \Exception {}

/**
 * Class FilterVar
 * @package Doof\Validate\Rules
 */
class FilterVar extends Rule
{

    /** @var array[string]int */
    const FILTERS = [
        'boolean' => FILTER_VALIDATE_BOOLEAN,
        'email' => FILTER_VALIDATE_EMAIL,
        'float' => FILTER_VALIDATE_FLOAT,
        'int' => FILTER_VALIDATE_INT,
        'ip' => FILTER_VALIDATE_IP,
        'mac' => FILTER_VALIDATE_MAC,
        'regexp' => FILTER_VALIDATE_REGEXP,
        'url' => FILTER_VALIDATE_URL,
    ];

    /**
     * @param \Closure|\mixed[]|string $rule
     * @throws \Doof\Validate\Rules\InvalidFilterException
     */
    public function __construct($rule)
    {
        if (self::isValidFilter($rule))
        {
            parent::__construct($rule);
        }
        else
        {
            throw new InvalidFilterException;
        }
    }

    /**
     * @param string $input
     * @return bool
     */
    public function isValid($input)
    {
        $filters = self::FILTERS;
        return (bool) filter_var($input, $filters[$this->rule]);
    }

    /**
     * @param $filter
     * @return bool
     */
    final public static function isValidFilter($filter)
    {
        return in_array($filter, array_keys(self::FILTERS));
    }

}