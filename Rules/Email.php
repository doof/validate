<?php

namespace Doof\Validate\Rules;

use Doof\Validate\Rules\FilterVar;

/**
 * Class Email
 * @package Doof\Validate\Rules
 */
class Email extends FilterVar
{

    public function __construct()
    {
        parent::__construct('email');
    }

}